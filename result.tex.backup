%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% "ModernCV" CV and Cover Letter
% LaTeX Template
% Version 1.3 (29/10/16)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% Original author:
% Xavier Danaux (xdanaux@gmail.com) with modifications by:
% Vel (vel@latextemplates.com)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
% Important note:
% This template requires the moderncv.cls and .sty files to be in the same 
% directory as this .tex file. These files provide the resume style and themes 
% used for structuring the document.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[11pt,a4paper,sans]{moderncv} % Font sizes: 10, 11, or 12; paper sizes: a4paper, letterpaper, a5paper, legalpaper, executivepaper or landscape; font families: sans or roman

\moderncvstyle{casual} % CV theme - options include: 'casual' (default), 'classic', 'oldstyle' and 'banking'
\moderncvcolor{blue} % CV color - options include: 'blue' (default), 'orange', 'green', 'red', 'purple', 'grey' and 'black'
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lipsum} % Used for inserting dummy 'Lorem ipsum' text into the template

\usepackage[scale=0.75]{geometry} % Reduce document margins
%\setlength{\hintscolumnwidth}{3cm} % Uncomment to change the width of the dates column
%\setlength{\makecvtitlenamewidth}{10cm} % For the 'classic' style, uncomment to adjust the width of the space allocated to your name

%----------------------------------------------------------------------------------------
%	NAME AND CONTACT INFORMATION SECTION
%----------------------------------------------------------------------------------------

\firstname{Wendell} % Your first name
\familyname{Fioravante da Silva Diniz} % Your last name

% All information in this block is optional, comment out any lines you don't need
\title{Curriculum Vitae}
\address{Rua Léo Club, 394}{Varginha, MG 37026-140}
\mobile{(19) 98394-4240}
\phone{}
% \fax{(000) 111 1113}
\email{wdiniz@fem.unicamp.br}
% \homepage{buscacv.cnpq.br/buscacv/\#/espelho?nro\_id\_cnpq\_cp\_s=5104764113732271}{home} % The first argument is the url for the clickable link, the second argument is the url displayed in the template - this allows special characters to be displayed such as the tilde in this example
% \extrainfo{additional information}
\photo[70pt][0.4pt]{identite5} % The first bracket is the picture height, the second is the thickness of the frame around the picture (0pt for no frame)
% \quote{"A witty and playful quotation" - John Smith}

%----------------------------------------------------------------------------------------

\begin{document}

%----------------------------------------------------------------------------------------
%	COVER LETTER
%----------------------------------------------------------------------------------------

% To remove the cover letter, comment out this entire block

% \clearpage
% 
% \recipient{HR Department}{Corporation\\123 Pleasant Lane\\12345 City, State} % Letter recipient
% \date{\today} % Letter date
% \opening{Dear Sir or Madam,} % Opening greeting
% \closing{Sincerely yours,} % Closing phrase
% \enclosure[Attached]{curriculum vit\ae{}} % List of enclosed documents
% 
% \makelettertitle % Print letter title
% 
% \lipsum[1-2] % Dummy text
% \lipsum[4] % Dummy text
% 
% \makeletterclosing % Print letter signature
% 
% \newpage

%----------------------------------------------------------------------------------------
%	CURRICULUM VITAE
%----------------------------------------------------------------------------------------

\makecvtitle % Print the CV title
Currículo Lattes:~\httplink[lattes.cnpq.br/5104764113732271]{buscacv.cnpq.br/buscacv/\#/espelho?nro\_id\_cnpq\_cp\_s=5104764113732271}
%----------------------------------------------------------------------------------------
%	PRESENTATION
%----------------------------------------------------------------------------------------

\section{Resumo}

\cvitem{}{Possui Bacharelado em Ciência da Computação pelo Centro Universitário do Sul de Minas/UNIS-MG (2009) Mestre em Engenharia Mecânica - Área de Concentração Mecânica dos Sólidos e Projeto Mecânico pelo Departamento de Mecânica Computacional da Faculdade de Engenharia Mecânica da Universidade Estadual de Campinas - UNICAMP (2012). Doutor em Engenharia Mecânica e Engenharia de Computação em programa de cotutela com dupla diplomação entre a Universidade Estadual de Campinas (Brasil) e a Université de Technologie de Compiègne, França (2017).   Interesses de Pesquisa  - Inteligência Artificial - Processamento de Imagens - Sistemas Embarcados - Computação de alto-desempenho - Veículos Autônomos e Semi-autônomos}

%----------------------------------------------------------------------------------------
%	EDUCATION SECTION
%----------------------------------------------------------------------------------------

\section{Formação}

\cventry{2006-2009}{Bacharelado}{Bacharelado em Ciência da Computação}{Centro Universitário do Sul de Minas}{}{Título: \emph{Hardware e Software para a implementação de um Controlador Lógico Programável didático}\\Orientador: Juliano Coêlho Miranda\\}
\cventry{2010-2012}{Mestrado}{Engenharia Mecânica}{Universidade Estadual de Campinas}{}{Título: \emph{Acionamento de Dispositivos Robóticos através de Interface Natural em Realidade Aumentada}\\Orientador: Eurípedes Guilherme de Oliveira Nóbrega\\}
\cventry{2012-2017}{Doutorado}{Doutorado em Engenharia Mecânica}{Universidade Estadual de Campinas}{}{Título: \emph{Conception and realization of an FPGA-based framework for embedded systems applied to Optimum-path Forest classifier}\\Orientador: Eurípedes Guilherme de Oliveira Nóbrega\\}



\section{Publicações}

\subsection{Trabalhos em congressos}

\cventry{}{DINIZ, WENDELL F. S.}{FREMONT, VINCENT; FANTONI, ISABELLE; NOBREGA, EURIPEDES G. O.}{Evaluation of optimum path forest classifier for pedestrian detection}{4}{5}\cventry{}{Diniz, Wendell F. S.;DINIZ, WENDELL F. S.}{Agnus Azevedo Horta; Eurípedes Guilherme de Oliveira Nóbrega; Luiz Otávio Saraiva Ferreira}{Parallel Implementation of Grayscale Conversion in Graphics Hardware}{4}{5}

%----------------------------------------------------------------------------------------
%	WORK EXPERIENCE SECTION
%----------------------------------------------------------------------------------------

\section{Experience}

\subsection{Vocational}

\cventry{2012--Present}{1\textsuperscript{st} Year Analyst}{\textsc{Lehman Brothers}}{Los Angeles}{}{Developed spreadsheets for risk analysis on exotic derivatives on a wide array of commodities (ags, oils, precious and base metals), managed blotter and secondary trades on structured notes, liaised with Middle Office, Sales and Structuring for bookkeeping.
\newline{}\newline{}
Detailed achievements:
\begin{itemize}
\item Learned how to make amazing coffee
\item Finally determined the reason for \textsc{PC LOAD LETTER}:
\begin{itemize}
\item Paper jam
\item Software issues:
\begin{itemize}
\item Word not sending the correct data to printer
\item Windows trying to print in letter format
\end{itemize}
\item Coffee spilled inside printer
\end{itemize}
\item Broke the office record for number of kitten pictures in cubicle
\end{itemize}}

%------------------------------------------------

\cventry{2011--2012}{Summer Intern}{\textsc{Lehman Brothers}}{Los Angeles}{}{Rated "truly distinctive" for Analytical Skills and Teamwork.}

%------------------------------------------------

\subsection{Miscellaneous}

\cventry{2010--2011}{}{}{}{}{Spent some time finding myself. This was a courageous endeavour that didn't have a job title. It was quite important to my overall development though so I'm adding it to my CV. Also it explains the gap in my otherwise stellar CV.}

\cventry{2009--2010}{Computer Repair Specialist}{Buy More}{Burbank}{}{Worked in the Nerd Herd and helped to solve computer problems. Allowed me to become expert in all forms of martial arts and weaponry.}

%----------------------------------------------------------------------------------------
%	AWARDS SECTION
%----------------------------------------------------------------------------------------

\section{Awards}

\cvitem{2011}{School of Business Postgraduate Scholarship}
\cvitem{2010}{Top Achiever Award -- Commerce}

%----------------------------------------------------------------------------------------
%	COMPUTER SKILLS SECTION
%----------------------------------------------------------------------------------------

\section{Computer skills}

\cvitem{Basic}{\textsc{java}, Adobe Illustrator}
\cvitem{Intermediate}{\textsc{python}, \textsc{html}, \LaTeX, OpenOffice, Linux, Microsoft Windows}
\cvitem{Advanced}{Computer Hardware and Support}

%----------------------------------------------------------------------------------------
%	COMMUNICATION SKILLS SECTION
%----------------------------------------------------------------------------------------

\section{Communication Skills}

\cvitem{2010}{Oral Presentation at the California Business Conference}
\cvitem{2009}{Poster at the Annual Business Conference in Oregon}

%----------------------------------------------------------------------------------------
%	LANGUAGES SECTION
%----------------------------------------------------------------------------------------

\section{Languages}

\cvitemwithcomment{English}{Mothertongue}{}
\cvitemwithcomment{Spanish}{Intermediate}{Conversationally fluent}
\cvitemwithcomment{Dutch}{Basic}{Basic words and phrases only}

%----------------------------------------------------------------------------------------
%	INTERESTS SECTION
%----------------------------------------------------------------------------------------

\section{Interests}

\renewcommand{\listitemsymbol}{-~} % Changes the symbol used for lists

\cvlistdoubleitem{Piano}{Chess}
\cvlistdoubleitem{Cooking}{Dancing}
\cvlistitem{Running}

%----------------------------------------------------------------------------------------

\end{document}
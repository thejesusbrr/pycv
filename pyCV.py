#!/usr/bin/python
# -*- coding: UTF-8 -*-

import io
from string import Template
import subprocess
import xml.etree.ElementTree as ET
import HTMLParser

nivel = {'GRADUACAO':u'Bacharelado','MESTRADO':u'Mestrado','DOUTORADO':u'Doutorado','POS-DOUTORADO':u'Pós-doutorado','CURSO-TECNICO-PROFISSIONALIZANTE':u'Curso Técnico Profissionalizante'}

root = ET.parse('curriculo.xml').getroot()
titulos = list(root[0][2])
titulacao = list()
for i, item in enumerate(titulos):
    anos = unicode(item.get('ANO-DE-INICIO'))+'-'+unicode(item.get('ANO-DE-CONCLUSAO'))
    curso = unicode(item.get('NOME-CURSO'))
    instituicao = unicode(item.get('NOME-INSTITUICAO'))
    if(item.tag == 'GRADUACAO'):
        trabalho = unicode(item.get('TITULO-DO-TRABALHO-DE-CONCLUSAO-DE-CURSO'))
        orientador = unicode(item.get('NOME-DO-ORIENTADOR'))
        infos = u'Título: \emph{'+trabalho+'}\\\\Orientador: '+orientador+'\\\\'
        titulacao.append(u'\cventry{%s}{%s}{%s}{%s}{}{%s}' % (anos, nivel[item.tag],curso,instituicao,infos))
    elif(item.tag == 'POS-DOUTORADO'):
        if(item.get('FLAG-BOLSA') == 'SIM'):
            titulacao.append(u'\cventry{%s}{%s}{%s}{}{}{Bolsista do(a) %s}' % (anos,nivel[item.tag],instituicao,unicode(item.get('NOME-AGENCIA'))))
        else:
            titulacao.append(u'\cventry{%s}{%s}{%s}{}{}{}' % (anos,nivel[item.tag],instituicao))
    elif(item.tag == 'CURSO-TECNICO-PROFISSIONALIZANTE'):
        titulacao.append(u'\cventry{%s}{%s}{%s}{%s}{}{}' % (anos,nivel[item.tag],curso,HTMLParser.HTMLParser().unescape(instituicao)))
    else:
        trabalho = unicode(item.get('TITULO-DA-DISSERTACAO-TESE'))
        orientador = unicode(item.get('NOME-COMPLETO-DO-ORIENTADOR'))
        if(item.get('FLAG-BOLSA') == 'SIM'):
            infos = u'Título: \emph{'+trabalho+'}\\\\Orientador: '+orientador+'\\\\Bolsista do(a) '+unicode(item.get('NOME-AGENCIA'))
        else:
            infos = u'Título: \emph{'+trabalho+'}\\\\Orientador: '+orientador+'\\\\'
        titulacao.append(u'\cventry{%s}{%s}{%s}{%s}{}{%s}' % (anos, nivel[item.tag],curso,HTMLParser.HTMLParser().unescape(instituicao),infos))
formacao = ''
for value in titulacao:
    formacao += value+'\n'

pubnat = list(root[1])
pubs = dict()
pub = u''
for item in pubnat:
    if(item.tag == 'TRABALHOS-EM-EVENTOS'):
        pub += u'\subsection{Trabalhos em congressos}\n\n'
    elif(item.tag == 'ARTIGOS-PUBLICADOS'):
        pub += u'\subsection{Artigos completos em periódicos}\n\n'
    autores = u''
    for sub in item:
        titulo = u'\emph{'+unicode(sub[0].get('TITULO-DO-TRABALHO'))+u'}'
        autores = sub.findall('AUTORES')
        citaut = list()
        for autor in autores:
            citaut.append(autor.get('NOME-PARA-CITACAO'))
        first = u'\capitalisewords{'+unicode(citaut.pop(0)).lower()+u'}'
        others = u''
        for i,autor in enumerate(citaut):
            others += u'\capitalisewords{'+unicode(autor).lower()+u'}'
            if(i <> len(citaut)-1):
                others += u'; '
        evento = unicode(sub[1].get('NOME-DO-EVENTO'))
        pub +=u'\cventry{%s}{%s}{%s}{%s}{%s}{}' % (unicode(sub[0].get('ANO-DO-TRABALHO')),first,others,titulo,evento)

#for key, value in pubs.iteritems():
#    pub += value+'\n'

#formacao = u'\cventry{2012-2017}{Doutorado}{Unicamp}{Campinas}{Doutorado em Engenharia Mecânica}{Infos\\\\Mais infos}'

dados = {
    'identificador':root.get('NUMERO-IDENTIFICADOR'),
    'firstName':'Wendell',
    'familyName':'Fioravante da Silva Diniz',
    'fullName':'Wendell Fioravante da Silva Diniz',
    'enderecoResidencial':u'Rua Léo Club, 394',
    'cidade':'Varginha',
    'estado':'MG',
    'cep':'37026-140',
    'celular':'(19) 98394-4240',
    'fone':'',
    'email':root[0][1][0].get('E-MAIL'),
    'resumo':root[0][0].get('TEXTO-RESUMO-CV-RH'),
    'formacao':formacao,
    'publicacoes':pub
}

latexfile = io.open('main.tex', 'r',encoding='utf8').read()
template = Template(latexfile)

#print template.safe_substitute(values)
with io.open('result.tex','w',encoding='utf8') as f:
    f.write(template.safe_substitute(dados))
subprocess.call('pdflatex -interaction=nonstopmode result.tex >> /dev/null',shell=True)
